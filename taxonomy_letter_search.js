Drupal.behaviors.changeFilter = function (context) {

  $(".block-taxonomy_letter_search .glossary-filter ul li a", context).click(function(event) {

    event.preventDefault();
    id = $(this, context).parent().parent().parent().parent().parent().parent().attr('id');
	code = $(this, context).attr('rel');

    $("#" + id + " .glossary-result ul li a", context).each(function() {
	  rel = $(this, context).attr('rel');
	  if (code != rel) {
	    $(this, context).parent().hide();
	  } 
	  else {
		$(this, context).parent().show();
	  }
	});

  });
	
};
